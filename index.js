const PORT = 3000;
const koa = require('koa');
const acati = require('./src/acati');

const app = koa();

app.use(function* (next) {
	if (this.path !== '/acati') return yield next;
	this.body = acati;
});

app.use(function* () {
	this.status = 404;
	this.body = 'Not Found';
});

app.listen(PORT);
