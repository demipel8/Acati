module.exports = [
	{
		'id': 1,
		'name': 'Nellie Keijzer',
		'position': 'art director',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/1214/7229/1347/aca_portretten_nellie.jpg',
		'data-background-small': 'https://www.acato.nl/files/1214/7229/1347/aca_portretten_nellie.jpg'
	},
	{
		'id': 2,
		'name': 'Gijs van den Boom',
		'position': 'managing partner',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/7914/7229/1348/aca_portretten_gijs.jpg',
		'data-background-small': 'https://www.acato.nl/files/7914/7229/1348/aca_portretten_gijs.jpg'
	},
	{
		'id': 3,
		'name': 'Lodewijk van der Meer',
		'position': 'managing partner',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/7414/7229/1346/aca_portretten_lodewijk.jpg',
		'data-background-small': 'https://www.acato.nl/files/7414/7229/1346/aca_portretten_lodewijk.jpg'
	},
	{
		'id': 4,
		'name': 'Pieter van Leeuwen',
		'position': 'user experience',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/2914/7229/1346/aca_portretten_pieter.jpg',
		'data-background-small': 'https://www.acato.nl/files/2914/7229/1346/aca_portretten_pieter.jpg'
	},
	{
		'id': 5,
		'name': 'Grietje Bruijntjes',
		'position': 'planning, HR & recruiting',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/1014/7229/1347/aca_portretten_grietje.jpg',
		'data-background-small': 'https://www.acato.nl/files/1014/7229/1347/aca_portretten_grietje.jpg'
	},
	{
		'id': 6,
		'name': 'Swie Oei',
		'position': 'Team Lead',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/6414/7229/1347/aca_portretten_swie.jpg',
		'data-background-small': 'https://www.acato.nl/files/6414/7229/1347/aca_portretten_swie.jpg'
	},
	{
		'id': 7,
		'name': 'Nina van der Scheer',
		'position': 'office management',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/7314/7229/1348/aca_portretten_nina.jpg',
		'data-background-small': 'https://www.acato.nl/files/7314/7229/1348/aca_portretten_nina.jpg'
	},
	{
		'id': 8,
		'name': 'Patrick Teulings',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/9714/7229/1345/aca_portretten_patrick.jpg',
		'data-background-small': 'https://www.acato.nl/files/9714/7229/1345/aca_portretten_patrick.jpg'
	},
	{
		'id': 9,
		'name': 'Brian van Delden',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/7914/7229/1347/aca_portretten_brian.jpg',
		'data-background-small': 'https://www.acato.nl/files/7914/7229/1347/aca_portretten_brian.jpg'
	},
	{
		'id': 10,
		'name': 'Colin Spoel',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/8014/7229/1345/aca_portretten_colin.jpg',
		'data-background-small': 'https://www.acato.nl/files/8014/7229/1345/aca_portretten_colin.jpg'
	},
	{
		'id': 11,
		'name': 'Valentijn de Jong',
		'position': 'managing partner',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/6614/7229/1346/aca_portretten_valentijn.jpg',
		'data-background-small': 'https://www.acato.nl/files/6614/7229/1346/aca_portretten_valentijn.jpg'
	},
	{
		'id': 12,
		'name': 'Joep',
		'position': 'kwispelaar',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/1114/7229/1346/aca_portretten_joep.jpg',
		'data-background-small': 'https://www.acato.nl/files/1114/7229/1346/aca_portretten_joep.jpg'
	},
	{
		'id': 13,
		'name': 'Rogier van der Heijden',
		'position': 'interactieontwerp',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/1414/7229/1347/aca_portretten_rogier.jpg',
		'data-background-small': 'https://www.acato.nl/files/1414/7229/1347/aca_portretten_rogier.jpg'
	},
	{
		'id': 14,
		'name': 'Sophie Lotgering',
		'position': 'accountlead & online strategie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/3314/7229/1345/aca_portretten_sophie.jpg',
		'data-background-small': 'https://www.acato.nl/files/3314/7229/1345/aca_portretten_sophie.jpg'
	},
	{
		'id': 15,
		'name': 'Kevin Poot',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/9014/7229/1348/aca_portretten_kevin.jpg',
		'data-background-small': 'https://www.acato.nl/files/9014/7229/1348/aca_portretten_kevin.jpg'
	},
	{
		'id': 16,
		'name': 'Andrée Lange',
		'position': 'ontwerp',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/6614/7229/1345/aca_portretten_andree.jpg',
		'data-background-small': 'https://www.acato.nl/files/6614/7229/1345/aca_portretten_andree.jpg'
	},
	{
		'id': 17,
		'name': 'Jorik Bosman',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/2014/7229/1345/aca_portretten_jorik.jpg',
		'data-background-small': 'https://www.acato.nl/files/2014/7229/1345/aca_portretten_jorik.jpg'
	},
	{
		'id': 18,
		'name': 'Corné Guijt',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/9214/7229/1346/aca_portretten_corne.jpg',
		'data-background-small': 'https://www.acato.nl/files/9214/7229/1346/aca_portretten_corne.jpg'
	},
	{
		'id': 19,
		'name': 'Marien van Overbeek',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/9314/7229/1348/aca_portretten_marien.jpg',
		'data-background-small': 'https://www.acato.nl/files/9314/7229/1348/aca_portretten_marien.jpg'
	},
	{
		'id': 20,
		'name': 'Jerry Duijm',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/8814/7229/1346/aca_portretten_jerry.jpg',
		'data-background-small': 'https://www.acato.nl/files/8814/7229/1346/aca_portretten_jerry.jpg'
	},
	{
		'id': 21,
		'name': 'Ramon Mostert',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/4614/7229/1346/aca_portretten_ramon.jpg',
		'data-background-small': 'https://www.acato.nl/files/4614/7229/1346/aca_portretten_ramon.jpg'
	},
	{
		'id': 22,
		'name': 'Chantal Pieters',
		'position': 'office management',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/5914/7229/1347/aca_portretten_chantal.jpg',
		'data-background-small': 'https://www.acato.nl/files/5914/7229/1347/aca_portretten_chantal.jpg'
	},
	{
		'id': 23,
		'name': 'Richard van Hees',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/8814/7229/1347/aca_portretten_richard_1.jpg',
		'data-background-small': 'https://www.acato.nl/files/8814/7229/1347/aca_portretten_richard_1.jpg'
	},
	{
		'id': 24,
		'name': 'Marit Vervaart',
		'position': 'online marketing strateeg',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/8114/7229/1348/aca_portretten_marit.jpg',
		'data-background-small': 'https://www.acato.nl/files/8114/7229/1348/aca_portretten_marit.jpg'
	},
	{
		'id': 25,
		'name': 'Chloé Rice',
		'position': 'ontwerp',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/3614/7229/1346/aca_portretten_chloe2.jpg',
		'data-background-small': 'https://www.acato.nl/files/3614/7229/1346/aca_portretten_chloe2.jpg'
	},
	{
		'id': 26,
		'name': 'Amber Taal',
		'position': 'online marketing strateeg',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/1514/7229/1347/aca_portretten_amber.jpg',
		'data-background-small': 'https://www.acato.nl/files/1514/7229/1347/aca_portretten_amber.jpg'
	},
	{
		'id': 27,
		'name': 'Michaël van Oosten',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/9114/7229/1345/aca_portretten_michael.jpg',
		'data-background-small': 'https://www.acato.nl/files/9114/7229/1345/aca_portretten_michael.jpg'
	},
	{
		'id': 28,
		'name': 'Mark Verkerk',
		'position': 'accountlead',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/7914/7229/1345/aca_portretten_mark.jpg',
		'data-background-small': 'https://www.acato.nl/files/7914/7229/1345/aca_portretten_mark.jpg'
	},
	{
		'id': 29,
		'name': 'Lexington Baly',
		'position': 'ontwerp en animatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/9214/7229/1346/aca_portretten_lexington.jpg',
		'data-background-small': 'https://www.acato.nl/files/9214/7229/1346/aca_portretten_lexington.jpg'
	},
	{
		'id': 30,
		'name': 'Richard Korthuis',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/8014/7229/1347/aca_portretten_richardk.jpg',
		'data-background-small': 'https://www.acato.nl/files/8014/7229/1347/aca_portretten_richardk.jpg'
	},
	{
		'id': 31,
		'name': 'Marieke Morsch',
		'position': 'accountlead',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/6914/7375/2234/aca_portretten_marieke.png',
		'data-background-small': 'https://www.acato.nl/files/6914/7375/2234/aca_portretten_marieke.png'
	},
	{
		'id': 32,
		'name': 'Jan Willem Oostendorp',
		'position': 'realisatie',
		'style': 'background-size: 186px 186px;',
		'data-background': 'https://www.acato.nl/files/5114/7375/2360/aca_portretten_Jan_willem.png',
		'data-background-small': 'https://www.acato.nl/files/5114/7375/2360/aca_portretten_Jan_willem.png'
	}
];
